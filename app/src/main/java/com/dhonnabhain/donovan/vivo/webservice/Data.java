package com.dhonnabhain.donovan.vivo.webservice;

/**
 * Created by donovan on 08/03/2017.
 */

import java.util.ArrayList;

public class Data {
    public boolean loaded = false;

    public ArrayList<Artist> artists = new ArrayList<Artist>();
    //public ArrayList<Concert> concerts = new ArrayList<Concert>();
    //public Playlist playlist = new Playlist();

    // Reset les données
    public void reset(){
        artists.clear();
    }
}
