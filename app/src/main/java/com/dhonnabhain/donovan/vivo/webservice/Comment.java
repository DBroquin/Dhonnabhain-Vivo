package com.dhonnabhain.donovan.vivo.webservice;

/**
 * Created by donovan on 13/03/2017.
 */

import java.util.ArrayList;

public class Comment {
    public String name;
    public String content;
    public String author;

    @Override
    public String toString()
    {
        return name;
    }
}
