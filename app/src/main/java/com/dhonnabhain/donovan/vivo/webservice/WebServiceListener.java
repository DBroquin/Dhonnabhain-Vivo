package com.dhonnabhain.donovan.vivo.webservice;

/**
 * Created by donovan on 08/03/2017.
 */

public interface WebServiceListener {
    public void onSuccess();
    public void onError(String message);
}
