package com.dhonnabhain.donovan.vivo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.HorizontalScrollView;

import java.util.ArrayList;
import java.util.StringTokenizer;

import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dhonnabhain.donovan.vivo.helpers.CircleTransform;
import com.dhonnabhain.donovan.vivo.webservice.Artist;
import com.dhonnabhain.donovan.vivo.webservice.WebService;
import com.squareup.picasso.Picasso;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        TextView topLabel = (TextView)findViewById(R.id.topLabel);
        topLabel.setText("Vivo");

        createApercu();
    }

    public void createApercu(){
        HorizontalScrollView artistScroll = (HorizontalScrollView)findViewById(R.id.artistScroll);

        LinearLayout artists = new LinearLayout(this);
        artists.setOrientation(LinearLayout.HORIZONTAL);

        ArrayList<Artist> listeArtistes = WebService.getAll();

        int index = 0;

        for(Artist artist: listeArtistes){

            final String indexArt = Integer.toString(index++);

            final String artName = artist.name;
            final HomeActivity self = this;

            View vue = getLayoutInflater().inflate(R.layout.art, null);

            ImageView pic = (ImageView) vue.findViewById(R.id.pic);
            Picasso.with(this.getApplicationContext()).load(artist.images.get(0).link).transform(new CircleTransform()).into(pic);
            pic.setClickable(true);

            // Evenement au click
            pic.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(self, ArtistActivity.class);
                    intent.putExtra("artist", artName);
                    intent.putExtra("artistId", indexArt);
                    startActivity(intent);
                }
            });

            TextView name = (TextView) vue.findViewById(R.id.name);
            name.setText(artist.name);

            artists.addView(vue);
            index++;
        }
        artistScroll.addView(artists);
    }

}
