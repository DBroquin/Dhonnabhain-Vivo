package com.dhonnabhain.donovan.vivo.webservice;

/**
 * Created by donovan on 08/03/2017.
 */

import java.util.ArrayList;

public class Album {
    public int id;

    public String name;
    public String cover;
    public String releaseDate;
    public String label;
    public String style;

    public ArrayList<Song> songs = new ArrayList<Song>();
    public ArrayList<Comment> comments = new ArrayList<Comment>();


    @Override
    public String toString()
    {
        return name;
        //return "\n\t\t Album : " + title + "\n\t\t\ttracks: " + tracks + "\n";
    }
}
